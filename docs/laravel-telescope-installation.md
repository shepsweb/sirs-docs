* Install following instructions in documentation: https://laravel.com/docs/master/telescope
* Keep (most of the) config defaults
	* disable CacheWatcher (see documentation on how to do this)
* Update the following two methods in TelescopeServiceProvider:
    * `TelescopeServiceProvider::gate()`
    	* define what kind of user should have access to the Telescope dashboard

    * `TelescopeServiceProvider::register()`
    	* modify default definition to log everything regardless of environment

```
protected function gate(): void
{
  Gate::define('viewTelescope', function ($user) {
  	// update as needed
    return $user->hasRole('programmer');
  });
}
```

```
public function register(): void
{
	[...]

    Telescope::filter(function (IncomingEntry $entry) {
    	// Default code looks something like this...
    	// return $isLocal ||
        //       $entry->isReportableException() ||
        //       $entry->isFailedRequest() ||
        //       $entry->isFailedJob() ||
        //       $entry->isScheduledTask() ||
        //       $entry->hasMonitoredTag();

        // Change to log everything regardless of environment
        return true;
    });

    [...]
}
```