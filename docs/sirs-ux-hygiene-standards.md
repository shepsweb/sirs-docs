# SIRS UX Hygiene Standards

## General
* All screens should have intro/how-to-use info readily available
* UI should provide feedback about actions that have been completed/failed
* User should see loading feedback immediately on initiating a ajax call


## Forms
### General Feedback
* User should see feedback that the form is being saved (?)
* UI should prevent user from "double-submitting" (?)

### Validation
* Validation errors should be reported in proximity to the fields they reference (?)
* Validation should be checked before the form is submitted to the server when possible (?)
* All entered data should be pre-populated in the case of validation errors (on round trip validation)

### Lists