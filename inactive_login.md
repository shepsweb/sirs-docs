**This all assumes you are using the Laravel-UI package**

Laravel's `LoginController` (App\Http\Controllers\Auth\LoginController.php) uses a trait called `AuthenticatesUsers` (Illuminate\Foundation\Auth\AuthenticatesUsers.php) to handle validating credentials and logging in a user during a login request. When attempting to validate a user, the trait only checks user's username and password, and so we need to instruct the trait to *also* check a user's `is_active` value.

In your app's LoginController.php file, copy/paste the `credentials()` method from the AuthenticatesUsers trait. Merge your user status validation into this array.

```
protected function credentials(Request $request)
    {
        // Check the actual vendor file for the real code
        // return merge [$credentials, 'is_active' => 1];
    }
```

This should be all you need to change to verify users are active during a login request. 

IF you are using onyen auth or some kind of authentication that is NOT through the standard login POST request, you also need to update that code. Updating `credentials()` only works with the LOGIN FORM.

-----

Longer explanation of how it works:

POST request to Illuminate\Foundation\Auth\AuthenticatesUsers::login()

AuthenticatesUsers::login() calls AuthenticatesUsers::attemptLogin($request)

AuthenticatesUsers::attemptLogin() passes the return value of AuthenticatesUsers::credentials() into its guard driver's attempt() method. Here, our guard is the web, and the driver for the web guard is the SessionGuard. You can see this defined in the config/auth.php file. I will refer to the SessionGuard methods, but in reality they are the methods from whatever class is used as the guard driver.

In the SessionGuard::attempt() method, our $credentials array is passed in and used to retrieve the $user in a call to the provider's retrieveByCredentials() method. The provider for the web guard is EloquentUserProvider class. Again, these are configured in config/auth.php. This user and the credentials are then passed to the SessionGuard::hasValidCredentials() method. In SessionGuard::hasValidCredentials(), it will return false if the user is null (among other condition checks). THIS is finally where our code changes came into play. In the call to retrieveByCredentials(), the return value of our query for an inactive user is null because we are looking for a user whose is_active value is 1.


https://itnext.io/laravel-the-power-of-authentication-part-2-f2386eaebcad
