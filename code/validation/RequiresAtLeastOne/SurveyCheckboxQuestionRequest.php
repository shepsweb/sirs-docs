<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\RequiresAtLeastOne;

class SurveyCheckboxQuestionRequest extends FormRequest
{
    /**
     * In this example question, a respondent is asked to specify what health insurance coverage they had and to mark all that apply.
     * Checking at least one box is required.
     *
     * ## Challenge
     *
     * Standard validation for checkboxes in Laravel only applies to individual options. We need to ensure that at least one checkbox is selected for the entire question group.
     *
     * ## Solution
     *
     * 1. **Custom Validation Rule:** A custom rule named `RequiresAtLeastOne` is created. This rule takes the names of all checkbox options within the group as arguments.
     * 2. **Validation Check:** Inside the `RequiresAtLeastOne` rule, the request body is inspected to see if at least one checkbox option (identified by its name) has a value (i.e., is checked).
     * 3. **Rule Application:**  In the request class `rules` method, a key named `INS_group` is added to the validation rules array. The value for this key is an instance of the `RequiresAtLeastOne` rule, passing the list of all checkbox field names (`$insuranceFields`) as arguments.
     * 4. **Error Handling:** If no options are checked, the validation fails for the `INS_group` key, not the individual checkbox fields.
     * 5. **User Feedback:** A single error message indicating "Please select at least one option." is added to the error bag for the `INS_group` key. This message can be displayed for the entire question group on the front end.
     * 6. Additional validation on individual fields can still be applied to the checkbox fields within the rules() method.
     * 
     */

    /*
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $insuranceFields = [
            'INS_EMPLOYER',
            'INS_MEDICAID',
            'INS_NCHC',
            'INS_MEDICARE',
            'INS_MILITARY',
            'INS_MARKETPLACE',
            'INS_OTHER',
            'INS_UNINSURED',
        ];

        return [
            'INS_group' => [new RequiresAtLeastOne('INS_group', $insuranceFields)],
            'INS_EMPLOYER' => 'nullable',
            'INS_MEDICAID' => 'nullable',
            'INS_NCHC' => 'nullable',
            'INS_MEDICARE' => 'nullable',
            'INS_MILITARY' => 'nullable',
            'INS_MARKETPLACE' => 'nullable',
            'INS_OTHER' => 'nullable',
            'INS_UNINSURED' => 'nullable',
        ];
    }
}
