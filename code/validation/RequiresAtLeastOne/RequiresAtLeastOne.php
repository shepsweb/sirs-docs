<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;

class RequiresAtLeastOne implements DataAwareRule, ValidationRule
{
    protected $fieldGroup;

    protected $fields = [];

    protected $data = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($fieldGroup, $fields)
    {
        $this->fieldGroup = $fieldGroup;
        $this->fields = $fields;
    }

    /**
     * Set the data under validation.
     */
    public function setData(array $data): static
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  mixed  $value
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $isValid = false;
        foreach ($this->fields as $field) {
            if (isset($this->data['response'][$field])) {
                $isValid = true;
            }
        }

        if ($isValid == false) {
            $fail('Please select at least one option.');
        }
    }
}
